﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AIGame
{
    public class MouseController : Entity
    {
        private GridMap GridMap { get; set; }

        private bool Dragging { get; set; }
        private GridType DraggingGridType { get; set; }
        private Point DraggingPosition { get; set; }

        private Texture2D CursorTexture { get; set; }

        public MouseController(GridMap gridMap)
        {
            GridMap = gridMap;

            Dragging = false;
        }

        public override void LoadContent()
        {
            CursorTexture = Game.Content.Load<Texture2D>("cursor");
        }

        public override void Update(float deltaSeconds)
        {
            if (Game.KeyboardController.Running) return;

            var mouseState = Mouse.GetState();

            var column = mouseState.X / GridController.GridWidth;
            var row = mouseState.Y / GridController.GridHeight;

            if (column < 0 || column >= GridMap.ColumnCount
                || row < 0 || row >= GridMap.RowCount)
                return;

            var grid = GridMap[column, row];

            bool leftButtonDown = (mouseState.LeftButton == ButtonState.Pressed);
            bool rightButtonDown = (mouseState.RightButton == ButtonState.Pressed);

            if (leftButtonDown && Dragging)
            {
                // set obstacle
                if (DraggingGridType == GridType.Obstacle && grid.GridType == GridType.None)
                    grid.GridType = GridType.Obstacle;
            }
            else if (leftButtonDown && !Dragging)
            {
                // drag NPC, target, or set obstacle
                Dragging = true;
                DraggingGridType = grid.GridType;
                if (DraggingGridType == GridType.None) DraggingGridType = GridType.Obstacle;

                if (DraggingGridType == GridType.NPC || DraggingGridType == GridType.Target)
                {
                    grid.GridType = GridType.None;
                    var posX = mouseState.X - column * GridController.GridWidth;
                    var posY = mouseState.Y - row * GridController.GridHeight;
                    DraggingPosition = new Point(posX, posY);
                }
            }
            else if (!leftButtonDown && Dragging)
            {
                // drop NPC or target
                Dragging = false;
                if (DraggingGridType == GridType.NPC || DraggingGridType == GridType.Target)
                {
                    var draggingCenterX = mouseState.X - DraggingPosition.X + GridController.GridWidth / 2;
                    var draggingCenterY = mouseState.Y - DraggingPosition.Y + GridController.GridHeight / 2;
                    var droppingX = draggingCenterX / GridController.GridWidth;
                    var droppingY = draggingCenterY / GridController.GridHeight;
                    var droppingGrid = GridMap[droppingX, droppingY];
                    droppingGrid.GridType = DraggingGridType;
                }
            }
            else if (rightButtonDown && grid.GridType == GridType.Obstacle)
            {
                // clear obstacle
                grid.GridType = GridType.None;
            }
        }

        public override void Draw(float deltaSeconds)
        {
            var mouseState = Mouse.GetState();
            var mousePosition = new Vector2(mouseState.X, mouseState.Y);

            if (Dragging && (DraggingGridType == GridType.NPC || DraggingGridType == GridType.Target))
            {
                var x = mousePosition.X - DraggingPosition.X;
                var y = mousePosition.Y - DraggingPosition.Y;
                var pos = new Vector2(x, y);
                Game.SpriteBatch.Draw(GridController.TextureTable[DraggingGridType], pos, Color.White);
            }

            Game.SpriteBatch.Draw(CursorTexture, mousePosition, Color.White);
        }
    }
}
