﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AIGame
{
    public class Monitor : Entity
    {
        private SpriteFont RealTimeDataFont { get; set; }
        private SpriteFont HelpFont { get; set; }

        private bool WasOperationKeyDown { get; set; }
        private bool WasMarkKeyDown { get; set; }
        private bool ShowingOperations { get; set; }
        private bool ShowingMark { get; set; }

        public Monitor()
        {
            WasOperationKeyDown = false;
            WasMarkKeyDown = false;
            ShowingOperations = false;
            ShowingMark = false;
        }

        public override void LoadContent()
        {
            RealTimeDataFont = Game.Content.Load<SpriteFont>("largeFont");
            HelpFont = Game.Content.Load<SpriteFont>("smallFont");
        }

        public override void Update(float deltaSeconds)
        {
            var keyState = Keyboard.GetState();

            var operationKeyDown = keyState.IsKeyDown(Keys.O);
            var markKeyDown = keyState.IsKeyDown(Keys.P);

            if (operationKeyDown && !WasOperationKeyDown) ShowingOperations = !ShowingOperations;
            if (markKeyDown && !WasMarkKeyDown) ShowingMark = !ShowingMark;

            WasOperationKeyDown = operationKeyDown;
            WasMarkKeyDown = markKeyDown;
        }

        public override void Draw(float deltaSeconds)
        {
            DrawRealTimeData();
            DrawHelp();
        }

        private void DrawRealTimeData()
        {
            var npcPosition = Game.GridController.GridMap.NPCPosition;
            var targetPosition = Game.GridController.GridMap.TargetPosition;

            var info = new List<String>();

            info.Add(string.Format("NPC position: {0}, {1}", npcPosition.X, npcPosition.Y));
            info.Add(string.Format("Target position: {0}, {1}", targetPosition.X, targetPosition.Y));

            var currentGrid = Game.KeyboardController.NPCController.Current;
            if (currentGrid != null)
            {
                var currentPosition = currentGrid.Position;
                info.Add(string.Format("Current position: {0}, {1}", currentPosition.X, currentPosition.Y));
            }

            var pos = new Vector2(20, 10);
            foreach (var str in info)
            {
                Game.SpriteBatch.DrawString(RealTimeDataFont, str, pos, Color.White);
                pos += new Vector2(0, 30);
            }
        }

        private void DrawHelp()
        {
            var info = new List<String>();

            if (ShowingOperations)
            {
                info.Add("O: Hide operations");
                if (Game.KeyboardController.Running)
                {
                    info.Add("  Space bar: Pause or continue");
                    info.Add("  Right arrow: Step");
                    info.Add("  Backspace: Stop and reset");
                }
                else
                {
                    info.Add("  Drag: Move NPC or target");
                    info.Add("  Left button: Set obstacles");
                    info.Add("  Right button: Clear obstacles");
                    info.Add("  Space bar: Begin and advance");
                    info.Add("             automatically");
                    info.Add("  Right arrow: Begin and advance");
                    info.Add("               step by step");
                }
            }
            else
            {
                info.Add("O: Show operations");
            }

            if (ShowingMark)
            {
                info.Add("P: Hide mark meanings");
                info.Add("  Green: NPC");
                info.Add("  Red: Target");
                info.Add("  Blue: Obstacle");
                info.Add("  Green frame: Open set");
                info.Add("  Blue frame: Closed set");
                info.Add("  Yellow frame: Path");
            }
            else
            {
                info.Add("P: Show mark meanings");
            }

            var pos = new Vector2(900, 10);
            foreach (var str in info)
            {
                Game.SpriteBatch.DrawString(HelpFont, str, pos, Color.White);
                pos += new Vector2(0, 30);
            }
        }
    }
}
