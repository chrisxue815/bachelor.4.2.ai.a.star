﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AIGame
{
    public class GridMap : IEnumerable<List<Grid>>
    {
        private List<List<Grid>> Grids { get; set; }

        public int ColumnCount { get; private set; }
        public int RowCount { get; private set; }

        private Point _npcPosition;
        public Point NPCPosition
        {
            get { return _npcPosition; }
            set
            {
                Grids[_npcPosition.X][_npcPosition.Y].GridType = GridType.None;
                Grids[value.X][value.Y].GridType = GridType.NPC;
                _npcPosition = value;
            }
        }

        private Point _targetPosition;
        public Point TargetPosition
        {
            get { return _targetPosition; }
            set
            {
                Grids[_targetPosition.X][_targetPosition.Y].GridType = GridType.None;
                Grids[value.X][value.Y].GridType = GridType.Target;
                _targetPosition = value;
            }
        }

        public Grid this[int column, int row]
        {
            get { return Grids[column][row]; }
            set
            {
                Grids[column][row] = value;
                switch (value.GridType)
                {
                    case GridType.NPC:
                        Grids[NPCPosition.X][NPCPosition.Y].GridType = GridType.None;
                        NPCPosition = new Point(column, row);
                        break;
                    case GridType.Target:
                        Grids[TargetPosition.X][TargetPosition.Y].GridType = GridType.None;
                        TargetPosition = new Point(column, row);
                        break;
                }
            }
        }

        public Grid this[Point point]
        {
            get { return this[point.X, point.Y]; }
            set { this[point.X, point.Y] = value; }
        }

        public Grid NPCGrid
        {
            get { return this[NPCPosition]; }
            set { this[NPCPosition] = value; }
        }

        public Grid TargetGrid
        {
            get { return this[TargetPosition]; }
            set { this[TargetPosition] = value; }
        }

        public GridMap(int columnCount, int rowCount)
        {
            ColumnCount = columnCount;
            RowCount = rowCount;

            Grids = new List<List<Grid>>(columnCount);

            for (var column = 0; column < columnCount; column++)
            {
                var columnList = new List<Grid>(rowCount);
                for (var row = 0; row < rowCount; row++)
                {
                    columnList.Add(new Grid(this, column, row, GridType.None));
                }
                Grids.Add(columnList);
            }

            NPCPosition = new Point(4, 6);
            TargetPosition = new Point(20, 6);
        }

        public void Reset()
        {
            foreach (var columnList in Grids)
            {
                foreach (var grid in columnList)
                {
                    grid.Reset();
                }
            }
        }

        public List<Grid> NeighborGrids(Grid grid)
        {
            var neighbors = new List<Grid>();

            int top = grid.Row - 1;
            int bottom = grid.Row + 1;
            int left = grid.Column - 1;
            int right = grid.Column + 1;

            // add grids from top-left clockwise
            AddIfValid(neighbors, left, top);
            AddIfValid(neighbors, grid.Column, top);
            AddIfValid(neighbors, right, top);
            AddIfValid(neighbors, right, grid.Row);
            AddIfValid(neighbors, right, bottom);
            AddIfValid(neighbors, grid.Column, bottom);
            AddIfValid(neighbors, left, bottom);
            AddIfValid(neighbors, left, grid.Row);

            return neighbors;
        }

        private void AddIfValid(List<Grid> list, int column, int row)
        {
            if (column >= 0 && column < ColumnCount && row >= 0 && row < RowCount)
            {
                var grid = Grids[column][row];
                if (grid.GridType != GridType.Obstacle)
                {
                    list.Add(grid);
                }
            }
        }

        public IEnumerator<List<Grid>> GetEnumerator()
        {
            return Grids.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
