﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AIGame
{
    public class PriorityQueue<T> : IEnumerable<T>
        where T : class, IComparable<T>
    {
        private List<T> Queue { get; set; }

        public PriorityQueue()
        {
            Queue = new List<T>();
        }

        public void Enqueue(T item)
        {
            Queue.Add(item);
        }

        public T Dequeue()
        {
            Queue = Queue.OrderByDescending(item => item).ToList();

            if (Queue.Count > 0)
            {
                var item = Queue.Last();
                Queue.RemoveAt(Queue.Count - 1);
                return item;
            }

            return null;
        }

        public bool Contains(T item)
        {
            return Queue.Contains(item);
        }

        public int Count { get { return Queue.Count; } }
        public bool Empty { get { return Queue.Count == 0; } }

        public T this[int i]
        {
            get { return Queue[i]; }
            set { Queue[i] = value; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Queue.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
