﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AIGame
{
    public class GridController : Entity
    {
        public GridMap GridMap { get; set; }

        public const int GridWidth = 50;
        public const int GridHeight = 50;

        public static Dictionary<GridType, Texture2D> TextureTable { get; set; }

        public GridController()
        {
            var viewport = Game.Graphics.GraphicsDevice.Viewport;
            var columnCount = viewport.Width / GridWidth;
            var rowCount = viewport.Height / GridHeight;

            GridMap = new GridMap(columnCount, rowCount);
        }

        public override void LoadContent()
        {
            TextureTable = new Dictionary<GridType, Texture2D>();

            TextureTable[GridType.None] = Game.Content.Load<Texture2D>("none");
            TextureTable[GridType.NPC] = Game.Content.Load<Texture2D>("npc");
            TextureTable[GridType.Target] = Game.Content.Load<Texture2D>("target");
            TextureTable[GridType.Obstacle] = Game.Content.Load<Texture2D>("obstacle");
        }

        public override void Update(float deltaSeconds)
        {
        }

        public override void Draw(float deltaSeconds)
        {
            for (var column = 0; column < GridMap.ColumnCount; column++)
            {
                for (var row = 0; row < GridMap.RowCount; row++)
                {
                    var grid = GridMap[column, row];
                    var texture = TextureTable[grid.GridType];

                    var posX = column * GridWidth;
                    var posY = row * GridHeight;
                    var pos = new Vector2(posX, posY);

                    Game.SpriteBatch.Draw(texture, pos, Color.White);
                }
            }
        }
    }
}
