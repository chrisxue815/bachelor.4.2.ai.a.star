﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AIGame
{
    public enum GridType { None, Target, NPC, Obstacle }

    public class Grid : IComparable<Grid>
    {
        public GridMap GridMap { get; set; }

        public int Column { get; private set; }
        public int Row { get; private set; }
        public Point Position { get; private set; }
        public GridType GridType { get; set; }

        public int GScore { get; set; }
        public int HScore { get; set; }
        public int FScore { get; set; }

        public Grid Parent { get; set; }

        public Grid(GridMap gridMap, int column, int row, GridType gridType)
        {
            GridMap = gridMap;
            Column = column;
            Row = row;
            GridType = gridType;

            Position = new Point(column, row);
            Reset();
        }

        public void Reset()
        {
            GScore = 0;
            HScore = 0;
            FScore = 0;
            Parent = null;
        }

        public List<Grid> NeighborGrids()
        {
            return GridMap.NeighborGrids(this);
        }

        public int CompareTo(Grid other)
        {
            return FScore - other.FScore;
        }
    }
}
