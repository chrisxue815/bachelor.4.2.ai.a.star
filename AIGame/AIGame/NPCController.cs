﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace AIGame
{
    public class NPCController
    {
        private GridMap GridMap { get; set; }

        public PriorityQueue<Grid> OpenSet { get; set; }
        public PriorityQueue<Grid> ClosedSet { get; set; }
        public List<Grid> Path { get; set; }
        public Grid Current { get; set; }

        public NPCController(GridMap gridMap)
        {
            GridMap = gridMap;

            Reset();
        }

        public void Reset()
        {
            OpenSet = new PriorityQueue<Grid>();
            ClosedSet = new PriorityQueue<Grid>();
            Path = new List<Grid>();
            GridMap.Reset();

            Current = null;
        }

        public bool Advance()
        {
            if (Current == null)
            {
                Current = GridMap.NPCGrid;
                Current.GScore = 0;
                Current.HScore = HeuristicEstimateOfDistance(Current.Position, GridMap.TargetPosition);
                Current.FScore = Current.GScore + Current.HScore;
                OpenSet.Enqueue(Current);
            }

            if (OpenSet.Count == 0) return false;
            if (Path.Count != 0) return true;

            Current = OpenSet.Dequeue();

            if (Current == GridMap.TargetGrid)
            {
                ReconstructPath();
            }

            ClosedSet.Enqueue(Current);
            foreach (var neighbor in Current.NeighborGrids())
            {
                if (ClosedSet.Contains(neighbor)) continue;

                bool tentativeIsBetter;
                int tentativeGScore = Current.GScore + DistanceBetween(Current.Position, neighbor.Position);

                if (!OpenSet.Contains(neighbor))
                {
                    OpenSet.Enqueue(neighbor);
                    tentativeIsBetter = true;
                }
                else if (tentativeGScore < neighbor.GScore)
                {
                    tentativeIsBetter = true;
                }
                else
                {
                    tentativeIsBetter = false;
                }

                if (tentativeIsBetter)
                {
                    neighbor.Parent = Current;
                    neighbor.GScore = tentativeGScore;
                    neighbor.HScore = HeuristicEstimateOfDistance(neighbor.Position, GridMap.TargetPosition);
                    neighbor.FScore = neighbor.GScore + neighbor.HScore;
                }
            }

            return true;
        }

        private static int DistanceBetween(Point p1, Point p2)
        {
            var width = Math.Abs(p1.X - p2.X);
            var height = Math.Abs(p1.Y - p2.Y);
            var shorter = width < height ? width : height;
            var dist = Math.Abs(width - height) + Math.Sqrt(2) * shorter;
            return (int)(dist * 10);
        }

        private static int HeuristicEstimateOfDistance(Point p1, Point p2)
        {
            var width = Math.Abs(p1.X - p2.X);
            var height = Math.Abs(p1.Y - p2.Y);
            var dist = width + height;
            return dist * 10;
        }

        public void ReconstructPath()
        {
            for (var current = GridMap.TargetGrid; current != null; current = current.Parent)
            {
                Path.Add(current);
            }
        }
    }
}
