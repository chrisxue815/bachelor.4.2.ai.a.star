﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AIGame
{
    public class KeyboardController : Entity
    {
        public NPCController NPCController { get; set; }

        public bool Running { get; set; }
        public bool AutoAdvance { get; set; }

        private const float Delay = 0.5f;
        private float DelayCounter { get; set; }
        private Keys[] PreviousKeys { get; set; }

        private Texture2D OpenSetTexture { get; set; }
        private Texture2D ClosedSetTexture { get; set; }
        private Texture2D PathTexture { get; set; }
        private SpriteFont Font { get; set; }

        public KeyboardController(GridMap gridMap)
        {
            NPCController = new NPCController(gridMap);
            Running = false;
            AutoAdvance = false;
            DelayCounter = 0;
        }

        public override void LoadContent()
        {
            OpenSetTexture = Game.Content.Load<Texture2D>("open");
            ClosedSetTexture = Game.Content.Load<Texture2D>("closed");
            PathTexture = Game.Content.Load<Texture2D>("path");
            Font = Game.Content.Load<SpriteFont>("tinyFont");
        }

        public override void Update(float deltaSeconds)
        {
            var keys = Keyboard.GetState().GetPressedKeys();

            foreach (var key in keys)
            {
                if (PreviousKeys.Contains(key)) continue;

                switch (key)
                {
                    case Keys.Back:
                        Running = false;
                        AutoAdvance = false;
                        NPCController.Reset();
                        break;
                    case Keys.Space:
                        Running = true;
                        AutoAdvance = !AutoAdvance;
                        if (AutoAdvance) DelayCounter = Delay;
                        break;
                    case Keys.Left:
                        break;
                    case Keys.Right:
                        Running = true;
                        NPCController.Advance();
                        break;
                }
            }

            PreviousKeys = keys;

            if (AutoAdvance)
            {
                DelayCounter -= deltaSeconds;
                if (DelayCounter <= 0)
                {
                    DelayCounter += Delay;
                    NPCController.Advance();
                }
            }
        }

        public override void Draw(float deltaSeconds)
        {
            foreach (var grid in NPCController.OpenSet)
            {
                DrawGrid(grid, OpenSetTexture);
            }

            foreach (var grid in NPCController.ClosedSet)
            {
                DrawGrid(grid, ClosedSetTexture);
            }

            foreach (var grid in NPCController.Path)
            {
                DrawGrid(grid, PathTexture);
            }
        }

        private void DrawGrid(Grid grid, Texture2D texture)
        {
            var posX = grid.Column * GridController.GridWidth;
            var posY = grid.Row * GridController.GridHeight;
            var pos = new Vector2(posX, posY);

            Game.SpriteBatch.Draw(texture, pos, Color.White);

            var fScoreStr = Convert.ToString(grid.FScore);
            var gScoreStr = Convert.ToString(grid.GScore);
            var hScoreStr = Convert.ToString(grid.HScore);

            var hScoreSize = Font.MeasureString(hScoreStr);
            var hScoreX = GridController.GridWidth - hScoreSize.X - 5;
            var hScoreY = GridController.GridHeight - hScoreSize.Y - 5;

            var fpos = pos + new Vector2(5, 5);
            var gpos = pos + new Vector2(5, hScoreY);
            var hpos = pos + new Vector2(hScoreX, hScoreY);

            Game.SpriteBatch.DrawString(Font, fScoreStr, fpos, Color.White);
            Game.SpriteBatch.DrawString(Font, gScoreStr, gpos, Color.White);
            Game.SpriteBatch.DrawString(Font, hScoreStr, hpos, Color.White);
        }
    }
}
