using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AIGame
{
    public class AIGame : Game
    {
        public static AIGame Instance { get; private set; }

        public GraphicsDeviceManager Graphics { get; private set; }
        public SpriteBatch SpriteBatch { get; private set; }

        public List<Entity> Entities { get; set; }
        public GridController GridController { get; set; }
        public MouseController MouseController { get; set; }
        public KeyboardController KeyboardController { get; set; }
        public Monitor Monitor { get; set; }

        public AIGame()
        {
            Instance = this;

            Content.RootDirectory = "Content";

            Graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = 1250,
                PreferredBackBufferHeight = 700
            };
        }

        protected override void Initialize()
        {
            GridController = new GridController();
            MouseController = new MouseController(GridController.GridMap);
            KeyboardController = new KeyboardController(GridController.GridMap);
            Monitor = new Monitor();

            Entities = new List<Entity>
            {
                GridController,
                MouseController,
                KeyboardController,
                Monitor
            };

            foreach (var entity in Entities)
            {
                entity.Initialize();
            }

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // create a SpriteBatch, and load the textures and font that we'll need
            // during the game.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            foreach (var entity in Entities)
            {
                entity.LoadContent();
            }

            base.LoadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            if (!IsActive) return;

            // Check for exit.
            var keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.Escape)) Exit();

            var deltaSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;

            foreach (var entity in Entities)
            {
                entity.Update(deltaSeconds);
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            if (!IsActive) return;

            var deltaSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;

            GraphicsDevice.Clear(Color.CornflowerBlue);

            SpriteBatch.Begin();

            foreach (var entity in Entities)
            {
                entity.Draw(deltaSeconds);
            }

            SpriteBatch.End();

            base.Draw(gameTime);
        }
    }

    static class Program
    {
        static void Main()
        {
            using (var game = new AIGame())
            {
                game.Run();
            }
        }
    }
}
